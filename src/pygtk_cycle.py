#
# main.py
# Copyright (C) 2019 reuben pygtk.posting@gmail.com
# 
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk, GLib
import psycopg2
from datetime import datetime
from dateutils import calendar_to_datetime

UI_FILE = "src/pygtk_cycle.ui"


class GUI:
	settings = None
	charts = None
	def __init__(self):

		self.builder = Gtk.Builder()
		self.builder.add_from_file(UI_FILE)
		self.builder.connect_signals(self)

		try:
			self.db = psycopg2.connect( database= 'cycle_tracker', 
										host= '192.168.0.120', 
										user='postgres', 
										password = 'true', 
										port = '5432')
			c = self.db.cursor()
			c.execute("CREATE TABLE IF NOT EXISTS event_types "
						"(id SERIAL PRIMARY KEY, "
						"name VARCHAR NOT NULL DEFAULT '', "
						"red double precision NOT NULL DEFAULT 0, "
						"green double precision NOT NULL DEFAULT 0, "
						"blue double precision NOT NULL DEFAULT 0, "
						"alpha double precision NOT NULL DEFAULT 1)")
			c.execute("CREATE TABLE IF NOT EXISTS events "
						"(id SERIAL PRIMARY KEY, "
						"event_date DATE NOT NULL DEFAULT now(), "
						"event_type BIGINT NOT NULL REFERENCES event_types ON DELETE RESTRICT, "
						"notes VARCHAR NOT NULL DEFAULT '')")
			c.execute("CREATE TABLE IF NOT EXISTS temperature "
						"(record_date DATE PRIMARY KEY DEFAULT now(), "
						"amount numeric (3, 1) )")
			c.execute("CREATE TABLE IF NOT EXISTS period_start_days "
						"(day DATE PRIMARY KEY)")
			self.db.commit()
		except Exception as e:
			print (e)
		self.day_detail_store = self.builder.get_object('day_detail_store')
		self.event_type_store = self.builder.get_object('event_type_store')
		self.populate_stores()
		calendar = self.builder.get_object('calendar')
		calendar.set_detail_func(self.calendar_func)
		date_time = str(datetime.today())
		year = date_time[0:4]
		month = date_time[5:7]
		day = date_time[8:10]
		calendar.select_month(int(month) - 1, int(year))
		calendar.select_day(int(day))

		box = self.builder.get_object('box2')
		self.popover = self.builder.get_object('popover')

		self.get_month_data()
		window = self.builder.get_object('window')
		window.show_all()
		window.maximize()

	def get_month_data (self):
		c = self.db.cursor()
		box = self.builder.get_object('history_box')
		c.execute("SELECT date_trunc('month', event_date) "
					"AS date_group FROM events "
					"GROUP BY date_group "
					"ORDER BY date_group")
		for row in c.fetchall():
			datetime = row[0]
			calendar = Gtk.Calendar()
			calendar.set_property('no_month_change', True)
			calendar.set_detail_width_chars(7)
			calendar.set_detail_height_rows(5)
			box.pack_start(calendar, False, False, 0)
			calendar.select_month(datetime.month - 1, datetime.year)
			calendar.select_day(0)
			calendar.connect('query-tooltip', self.tooltip)
			calendar.set_detail_func(self.calendar_func_basic)
		c.close()

	def settings_clicked (self, button):
		if not self.settings:
			import settings
			self.settings = settings.SettingsGUI(self, self.db)
		else:
			self.settings.present()

	def charts_clicked (self, button):
		if not self.charts:
			import charts
			self.charts = charts.ChartsGUI(self.db)
		else:
			self.charts.window.present ()

	def start_of_period_toggled (self, togglebutton):
		if self.populating == True:
			return
		c = self.db.cursor()
		if togglebutton.get_active () == True:
			c.execute("INSERT INTO period_start_days (day) VALUES (%s) "
						"ON CONFLICT DO NOTHING", 
						(self.date_time,))
		else:
			c.execute("DELETE FROM period_start_days WHERE day = %s", 
						(self.date_time,))
		self.db.commit()
		c.close()

	def delete_entry_clicked (self, button):
		selection = self.builder.get_object('day_detail_selection')
		model, path = selection.get_selected_rows()
		if path == []:
			return
		row_id = self.day_detail_store[path][0]
		c = self.db.cursor()
		c.execute("DELETE FROM events WHERE id = %s", (row_id,))
		self.db.commit()
		self.populate_day_detail_store ()
		c.close()

	def add_entry_clicked (self, button):
		c = self.db.cursor()
		c.execute("INSERT INTO events (event_type, event_date) VALUES "
					"((SELECT id FROM event_types ORDER BY id LIMIT 1), %s)",
						(self.date_time,))
		self.db.commit()
		self.populate_day_detail_store ()
		c.close()

	def event_type_changed (self, cellrenderercombo, path, treeiter):
		row_id = self.day_detail_store[path][0]
		event_type_id = self.event_type_store[treeiter][0]
		c = self.db.cursor()
		c.execute("UPDATE events SET event_type = %s WHERE id = %s", 
											(event_type_id, row_id))
		self.db.commit()
		self.populate_day_detail_store ()
		c.close()

	def notes_edited (self, cellrenderertext, path, text):
		row_id = self.day_detail_store[path][0]
		self.day_detail_store[path][4] = text
		c = self.db.cursor()
		c.execute("UPDATE events SET notes = %s WHERE id = %s", 
											(text, row_id))
		self.db.commit()
		c.close()

	def populate_stores (self):
		self.event_type_store.clear()
		c = self.db.cursor()
		c.execute("SELECT id::text, name "
							"FROM event_types "
							"ORDER BY name")
		for row in c.fetchall():
			self.event_type_store.append(row)
		c.close()
		
	def calendar_func (self, calendar, year, month, day):
		date = "%s %s %s" % (month+1, day, year)
		date_time = datetime.strptime(date, "%m %d %Y")
		string = str()
		c = self.db.cursor()
		c.execute("SELECT name, red, green, blue, alpha, notes FROM events "
					"JOIN event_types ON events.event_type = event_types.id "
					"WHERE event_date = %s ORDER BY name", (date_time,))
		for row in c.fetchall():
			name = row[0]
			red = row[1]
			green = row[2]
			blue = row[3]
			alpha = row[4]
			notes = row[5]
			hex_color = '#%02x%02x%02x%02x' %  (int(red*255),
												int(green*255),
												int(blue*255),
												int(alpha*255))
			string += "<span foreground='%s' weight='bold'>%s : %s</span>\n" % (
											hex_color, name, notes)
		c.close()
		return string
		
	def calendar_func_basic (self, calendar, year, month, day):
		current_month = calendar.get_date()[1]
		if current_month != month:
			return ''
		date = "%s %s %s" % (month+1, day, year)
		date_time = datetime.strptime(date, "%m %d %Y")
		string = str()
		c = self.db.cursor()
		c.execute("SELECT name, red, green, blue, alpha FROM events "
					"JOIN event_types ON events.event_type = event_types.id "
					"WHERE event_date = %s ORDER BY name", (date_time,))
		for row in c.fetchall():
			name = row[0]
			red = row[1]
			green = row[2]
			blue = row[3]
			alpha = row[4]
			hex_color = '#%02x%02x%02x%02x' %  (int(red*255),
												int(green*255),
												int(blue*255),
												int(alpha*255))
			string += "<span foreground='%s' weight='bold'>%s</span>\n" % (
											hex_color, name)
		c.close()
		return string
		
	def tooltip (self, calendar, x, y, keyboard_mode, tooltip):
		tooltip.set_text('Reuben')
		return True

	def spinbutton_focus_in (self, spinbutton, event):
		GLib.idle_add(spinbutton.select_region, 0, -1)

	def spinbutton_value_changed (self, spinbutton):
		temperature = spinbutton.get_value()
		c = self.db.cursor()
		c.execute("INSERT INTO temperature (record_date, amount) "
					"VALUES (%s, %s) ON CONFLICT (record_date) "
					"DO UPDATE SET amount = %s "
					"WHERE temperature.record_date = %s", 
					(self.date_time, temperature, temperature, self.date_time))
		c.close()
		self.db.commit()

	def day_selected (self, calendar):
		self.populating = True
		date = calendar.get_date()
		self.date_time = calendar_to_datetime (date)
		c = self.db.cursor()
		c.execute("SELECT true FROM period_start_days WHERE day = %s", 
					(self.date_time,))
		for row in c.fetchall():
			self.builder.get_object('start_of_period').set_active(True)
			break
		else:
			self.builder.get_object('start_of_period').set_active(False)
		c.close()
		self.populate_day_detail_store ()
		self.populating = False

	def populate_day_detail_store (self):
		self.day_detail_store.clear()
		c = self.db.cursor()
		c.execute("SELECT events.id, name, event_types.id, "
					"red, green, blue, alpha, notes "
					"FROM events "
					"JOIN event_types "
					"ON events.event_type = event_types.id "
					"WHERE event_date = %s ORDER BY name", 
					(self.date_time,))
		for row in c.fetchall():
			row_id = row[0]
			name = row[1]
			event_type = row[2]
			red = row[3]
			green = row[4]
			blue = row[5]
			alpha = row[6]
			notes = row[7]
			rgba = Gdk.RGBA(red, green, blue, alpha)
			self.day_detail_store.append([row_id, 
											name, 
											event_type, 
											rgba, 
											notes])
		c.close()
		self.db.rollback()

	def calendar_event (self, calendar, event):
		if event.type == Gdk.EventType.DOUBLE_BUTTON_PRESS:
			self.show_event_popup(calendar, event)
			return True # stop the signal propagation to show the popover

	def calendar_button_release_event (self, calendar, event):
		if event.button == 3:
			self.show_event_popup(calendar, event)

	def show_event_popup(self, calendar, event):
		c = self.db.cursor()
		c.execute("SELECT amount::text FROM temperature WHERE record_date = %s", 
					(self.date_time,))
		for row in c.fetchall():
			temperature = row[0]
			break
		else:
			temperature = '95.0'
		c.close()
		self.db.rollback()
		spinbutton = self.builder.get_object('spinbutton')
		GLib.idle_add(spinbutton.set_text, temperature)
		rect = Gdk.Rectangle()
		allocation = calendar.get_allocation()
		rect.x = event.x - allocation.x 
		rect.y = event.y - allocation.y
		rect.width = 1
		rect.heigth = 1
		self.popover.set_pointing_to (rect)
		self.popover.show_all ()

	def on_window_destroy(self, window):
		Gtk.main_quit()

