# settings.py
#
# Copyright (C) 2019 - reuben
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk

UI_FILE = "src/settings.ui"

class SettingsGUI:
	def __init__(self, main, db):

		self.builder = Gtk.Builder()
		self.builder.add_from_file(UI_FILE)
		self.builder.connect_signals(self)

		self.main = main 
		self.db = db
		self.cursor = self.db.cursor()
		self.new_event ()
		self.event_type_store = self.builder.get_object('event_type_store')
		self.populate_edit_tag_store()
		
		self.window = self.builder.get_object('window')
		self.window.show_all() 

	def populate_edit_tag_store (self):
		self.event_type_store.clear()
		self.cursor.execute("SELECT id, name, red, green, blue, alpha "
							"FROM event_types "
							"ORDER BY name")
		for row in self.cursor.fetchall():
			row_id = row[0]
			name = row[1]
			red = row[2]
			green = row[3]
			blue = row[4]
			alpha = row[5]
			rgba = Gdk.RGBA(red, green, blue, alpha)
			self.event_type_store.append([row_id, name, rgba])

	def save_clicked (self, button):
		name = self.builder.get_object('entry2').get_text()
		rgba = self.builder.get_object('colorbutton1').get_rgba()
		red = rgba.red
		green = rgba.green
		blue = rgba.blue
		alpha = rgba.alpha
		if self.tag_id == 0:
			self.cursor.execute ("INSERT INTO event_types "
								"(name, red, green, blue, alpha) VALUES "
								"(%s, %s, %s, %s, %s)", 
								(name, red, green, blue, alpha))
		else:
			self.cursor.execute ("UPDATE event_types SET "
								"(name, red, green, blue, alpha) = "
								"(%s, %s, %s, %s, %s) WHERE id = %s", 
								(name, red, green, blue, alpha, 
								self.tag_id))
		self.db.commit()
		self.populate_edit_tag_store ()
		self.main.populate_stores()

	def new_clicked (self, button):
		self.new_event()

	def new_event(self):
		rgba = Gdk.RGBA(0, 0, 0, 1)
		self.tag_id = 0 
		entry = self.builder.get_object('entry2')
		entry.set_text('New event')
		entry.select_region(0, -1)
		self.builder.get_object('colorbutton1').set_rgba(rgba)

	def row_activated (self, treeview, path, treeviewcolumn):
		self.tag_id = self.event_type_store[path][0]
		self.cursor.execute("SELECT name, red, green, blue, alpha "
							"FROM event_types "
							"WHERE id = %s", (self.tag_id,))
		for row in self.cursor.fetchall():
			name = row[0]
			red = row[1]
			green = row[2]
			blue = row[3]
			alpha = row[4]
			rgba = Gdk.RGBA(red, green, blue, alpha)
			self.builder.get_object('entry2').set_text(name)
			self.builder.get_object('colorbutton1').set_rgba(rgba)

	def present (self):
		self.window.present()

	def delete_event (self, window, event):
		window.hide()
		return True

