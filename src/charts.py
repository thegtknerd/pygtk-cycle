# charts.py
#
# Copyright (C) 2020 - Reuben
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as Canvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as Toolbar
import matplotlib.dates as mdates
from datetime import datetime

UI_FILE = "src/charts.ui"

class ChartsGUI(Gtk.Builder):
	def __init__(self, db):

		Gtk.Builder.__init__(self)
		self.add_from_file(UI_FILE)
		self.connect_signals(self)
		self.db = db
		self.window = self.get_object('window')
		self.window.show_all()

	def window_delete_event (self, window, event):
		window.hide()
		return True

	def temperature_chart_clicked (self, button):
		window = Gtk.Window()
		scrolled_window = Gtk.ScrolledWindow()
		window.add(scrolled_window)
		temperature_box = Gtk.Box()
		scrolled_window.add(temperature_box)
		full_date = self.get_object('full_date_checkbutton').get_active()
		date_format = mdates.DateFormatter('%b %d %Y')
		c = self.db.cursor()
		c.execute("WITH cte AS "
					"(SELECT day AS first_day "
						"FROM period_start_days ORDER BY day) "
					"SELECT first_day, "
						"(SELECT (day - INTERVAL'1 day')::date AS last_day "
						"FROM period_start_days "
						"WHERE day > cte.first_day ORDER BY day LIMIT 1) "
						"FROM cte ")
		for row in c.fetchall():
			box = Gtk.VBox()
			temperature_box.pack_start(box, True, True, 0)
			figure = Figure(figsize=(4, 4), dpi=100)
			canvas = Canvas(figure) 
			box.pack_start(canvas, True, True, 0)
			box.set_property('width-request', 1000)
			toolbar = Toolbar(canvas, window)
			box.pack_start(toolbar, False, False, 0)
			amount = list()
			days = list()
			first_day = row[0]
			last_day = row[1]
			if last_day == None:
				last_day = datetime.today().date()
			c.execute("WITH date_range AS "
							"(SELECT generate_series (%s, %s, '1 day' ) "
								"AS diary_date "
							") "
						"SELECT COALESCE(amount, 95.0), diary_date "
						"FROM date_range "
						"LEFT JOIN temperature "
							"ON record_date = date_range.diary_date "
						"ORDER BY date_range.diary_date", 
						(first_day, last_day))
			for day, row in enumerate(c.fetchall()):
				amount.append(row[0])
				if full_date:
					days.append(row[1])
				else:
					days.append(day + 1)
			subplot = figure.add_subplot(111)
			subplot.plot(days, amount)
			subplot.set_xticks(days)
			for tick in subplot.get_xticklabels():
				tick.set_rotation(90)
			if full_date:
				subplot.xaxis.set_major_formatter(date_format)
				figure.subplots_adjust(bottom=0.3)
			date_string = datetime.strftime(first_day, "%b %d %Y")
			subplot.set_title(date_string)
		c.close()
		window.set_size_request(400, 400)
		window.set_title ('Temperature chart')
		window.set_icon_name('folder-color-switcher-pink')
		window.show_all()
		self.db.rollback()
		self.window.hide()






